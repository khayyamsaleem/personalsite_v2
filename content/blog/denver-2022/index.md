---
title:  "Denver - Oct 2022"
date: 2023-05-20T21:59:20-04:00
tags:
  - travel
  - photography
---

## Queen City of the Plains

I arrived in Denver for a work offsite in October 2022. This was my first time in the "Mountain West" region of the USA, and I was fortunate to have excellent tours guided by my managers and coordinators at DigitalOcean.

## First, an ode to offsites

My time in Denver educated me about the true necessity offsites for remote workplaces. When I started at DigitalOcean, it was my first fully remote job. 
I had much to learn about what it takes to facilitate a productive remote culture at a company.
Office offsites are a vital ingredient for remote software teams, weaving together collaboration, innovation, and camaraderie.
These gatherings are crucial to forging deeper connections and friendships that enhance teamwork and productive output.
The face-to-face interactions fuel creativity and exchange of expertise, yielding more innovation back at the keyboard.
An offsite allows for a time for the team to arrive at the same page about goals and strategy. It allows for teams to shed any biases that arise
from the peculiarities of digital impressions. My conversations with my coworkers became immediately more authentic and empathetic.
Office offsites unlock the full potential of remote teams, producing enhanced collaboration, increased motivation, and a sense of belonging that 
truly can't be achieved over Zoom.

## Red Rocks Park and Amphitheatre

We had taken a big ole party bus to Red Rocks. I was really excited to see this place in person, having seen Bill Burr's "Live at Red Rocks" special earlier that year.

The stadium is massive and consuming. The natural cathedral envelopes you, feeling simultaneously massive and tucked-away.

Unfortunately, we didn't visit to see a show, so we weren't able to experience the renowned acoustic properties (aside from yelling "echo!" in the empty stadium).

However, the expansive vistas from the top offered a panoramic view unlike any that I'm familiar with in the Northeast. We were able to take a quaint hike on the 
outskirts of the park, and I took many pictures of the namesake rock formations in and around the stadium area.

{{< gallery dir="red-rocks" />}}

## Parks Downtown

### South Platte River Trail and Cherry Creek

The South Platte River Trail runs and bends along downtown Denver, offering a delightful escape from the city into nature's embrace.

The South Platte River Trail invites you on a scenic journey lined with lush greenery and wildlife, with the occasional sandy shore to take a break alongside
the water to listen to the sound of the flow.

Just beyond was a more intimate Cherry Creek trail. This one meanders alongside the creek, adorned with charming bridges, blooming flowers, and shaded groves. 

These two trails offered a gateway to tranquility at a walkable/bikable distance from the Hyatt Centric Downtown Denver where we stayed.

{{< gallery dir="south-platte-cherry-creek" />}}

### City Park

I don't have any photos of this area as I was too busy biking around, but this park was gorgeous, exhibiting the turn of the leaves for the Autumn.

## Places to eat

### Rioja - Larimer Square

This was the first of many excellent locations that our company treated us to.

Larimer Square is a historic and vibrant destination in Denver that exudes charm and character. Its iconic brick buildings and Victorian architecture create an inviting and picturesque atmosphere. The street is lined with a diverse array of boutique shops, upscale restaurants, and lively bars, making it a bustling hub of activity that captures the essence of Denver's rich cultural and culinary scene.

Here, we ate at [Rioja](https://riojadenver.com), a Mediterranean restaurant with an upscale feel in Larimer Square. Between artichoke tortelloni's, pea mousse ravioli, 
and squid ink pappardelle, the pastas at this restaurant really gave me the warm welcome I needed after a long journey to Denver.

### The Fort

Of the restaurants we visited, this was likely my favorite. At The Fort, we were treated to a true Colorado experience. This restaurant was on a sort-of campus of its
own, near the Red Rocks Amphitheatre. There were warming and pleasant fire pits all around the restaurant's outdoor spaces.

For me, the highlight was the stoic, cozy-looking bear statue, taking a seat on a log in front of a fire in the center courtyard of the restaurant.

Between high quality steaks, stuffed bell pepper, potatos, and the Mountain Man Toast ([not what you think](https://thefort.com/mountain-man-toast/)), The Fort is 
strongly memorable to me even now, as I write this reflection over six months after having left Denver.

{{< gallery dir="the-fort" />}}

### 16th St Mall

This block was nothing to phone home about, but it was nice to know that even in more sparse cities like Denver, there is a nice and bountiful
food street downtown. I was able to grab some quick meals between offsite sessions here, like a burrito from [Otra Vez Cantina](https://www.otravezcantina.com/), 
and a hearty rice bowl from [Motomaki](https://www.motomaki.com/).

{{< figure src="16th-st-mall.jpg" caption="16th St Mall">}}

## Public transportation

As usual, I really tried to make it work with Denver. When I arrived, I took the RTD (Regional Transportation District) metro train on the A line to get from
Denver International Airport to the plush Denver Union Station. This was a jarring transition for me, as I was just coming from an extended stay in West Europe, and
even for most of a typical year for me, I would stay in the New York Metro area where public transportation really met most of my needs.

Here, I was immediately subject to wait half an hour for the train to arrive, and another half after that for the train to leave the station. The airport was 23 miles 
away from the city, and the ride took about 40 minutes to complete. Though it was convenient to have a direct train from the airport to downtown (which is more than can
be said for NYC and JFK airport), it was cumbersome to have to wait so long at one of the busiest airports in the US.

One more enjoyable part of Denver's city infrastructure was the proliferation of the Lime electric bikes and scooters. Though the city truly is super sparse, I never had
any trouble or had to walk further than a few blocks to get my hands on an electric bike or scooter. This was a lifesaver during this trip, as I didn't have much time
between the offsite sessions to do much exploration of my own. With the help of the bikes and scooters, I was able to stop at a few coffee shops out of the way like
[Hooked on Colfax](https://www.hookedoncolfax.com/) and the famous [Tattered Cover Bookstore](https://www.tatteredcover.com/), and finally get a water bottle holder for my Fjallraven backpack.

I had convinced my coworkers to take a bus with me to our recreation event at [Punch Bowl Social](https://punchbowlsocial.com/location/denver/) (super fun!!!), and the
RTD app was quite convenient to use. However, it took the bus 18 minutes to go 2.3 miles, and it felt like we only got lucky because the route was convenient. I was
able to enjoy the walk back downtown though, as Denver feels quite safe even in the evening.

## Takeaways

As I flew away from Denver back to resume my loosely defined Europe tour, I knew that it wouldn't meet my needs as a place to live long-term.

It took only a short while to understand that the public transport connectivity and full-on embrace of urbanism that I desire in my target city was just 
not there. However, I will most definitely be willing and happy to return whenever I'm feeling that itch for nature and a break from the hustle of the 
Northeast, while still feeling like I'm in an urban center with tons to do.

{{< load-photoswipe >}}