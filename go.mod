module github.com/khayyamsaleem/personalsite_v2

go 1.18

require (
	github.com/khayyamsaleem/hugo-lightbox-gallery v0.0.0-20230521014959-c88845799b57 // indirect
	github.com/lgaida/mediumish-gohugo-theme v0.0.0-20230202155000-10fe85e58df4 // indirect
)
